//
//  ImageCollectionViewController.swift
//  PhotoFetcher
//
//  Created by Sumeet Gill on 2016-05-02.
//  Copyright © 2016 Sumeet Gill. All rights reserved.
//

import UIKit
import CoreData

private let reuseIdentifier = "ImageCell"
let numberOfImages:Int = 500

private let retrievedImages = [UIImage]()
private var retrievedLowResolution: [LowResolution] = []

private let manageObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext

class ImageCollectionViewController: UICollectionViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        retrievedLowResolution = retrieveImageFromCoreData("LowResolution") as! [LowResolution]
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return numberOfImages
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! ImageCollectionViewCell

        //Default image in case CoreData storage is empty
        var image = UIImage(named: "pupper")

        // Check to see if array is empty and if  current item is less than array size. Otherwise bad things happen
        if retrievedLowResolution.count > 0 && indexPath.item < retrievedLowResolution.count  {
            let lowResImage:LowResolution = retrievedLowResolution[indexPath.item]
            image = UIImage(data: lowResImage.imageData!)
        }
        
        cell.imageView.image = image
        
        return cell
    }
}

//
//  ImageCollectionViewCell.swift
//  PhotoFetcher
//
//  Created by Sumeet Gill on 2016-05-02.
//  Copyright © 2016 Sumeet Gill. All rights reserved.
//

import UIKit


class ImageCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var imageView: UIImageView!
    
    
}

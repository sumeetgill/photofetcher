//
//  CoreDataUtils.swift
//  PhotoFetcher
//
//  Created by Sumeet Gill on 2016-05-01.
//  Copyright © 2016 Sumeet Gill. All rights reserved.
//

import Foundation
import CoreData
import UIKit

private let coreDataQueue = dispatch_queue_create("coreDataQueue", DISPATCH_QUEUE_CONCURRENT)
private let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext

var lowResImages = []

func saveImageToCoreData(imageID: Int, lowResData:NSData, highResData:NSData) {
  
    dispatch_sync(coreDataQueue) { //Using a different thread for CoreData thread to try not disrupt UI performance

        guard let highRes = NSEntityDescription.insertNewObjectForEntityForName("HighResolution", inManagedObjectContext: managedObjectContext) as? HighResolution,
            let lowRes = NSEntityDescription.insertNewObjectForEntityForName("LowResolution", inManagedObjectContext: managedObjectContext) as? LowResolution else {
                print("Error: Managed context failed")
                return
        }
        
        highRes.imageData = highResData
        
        lowRes.imageData = lowResData
        lowRes.id = imageID
        
        do {
            try managedObjectContext.save()
            print("Successfully saved \(imageID) into CoreData")
        } catch {
            print("Error: managedContext could not save \(error)")
        }
        
        managedObjectContext.refreshAllObjects()
    }
}

func retrieveImageFromCoreData(name: String) ->  AnyObject {
    
    let fetchRequest = NSFetchRequest(entityName: name)
    fetchRequest.fetchLimit = numberOfImages
    
    do {
        lowResImages = try managedObjectContext.executeFetchRequest(fetchRequest)
        
    } catch {
        print("Oops! Error retrieving records from CoreData")
    }
    
    return lowResImages
}
//
//  ImageUtils.swift
//  PhotoFetcher
//
//  Created by Sumeet Gill on 2016-04-30.
//  Copyright © 2016 Sumeet Gill. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


private let request_url:String = "https://api.flickr.com/services/rest/"
private let request_method_id:String = "flickr.photos.getRecent"
private let request_method_url:String = "flickr.photos.getSizes"
private let response_format:String = "json"
private let api_key:String = "a54808ec981b2373e149d6a15e6dd2b3"

private var imageIDArray:[FlickrObject] = [] // Populated via getImageIDs and fed into imageURLToData

private let imageUtilsQueue = dispatch_queue_create("imageUtilsQueue", DISPATCH_QUEUE_CONCURRENT)

// Flickr requires us to get the image ID first, then query for the URL with another call
/// Number of images must be between 1 & 500.
func getImageIDs(numberOfImages: Int) {
    
    guard numberOfImages > 0 && numberOfImages < 501 else {
        print("Number of images requested is invalid. Must be greater than 0 and less than 500.")
        return
    }
    
    debugPrint("Number of images requested: \(numberOfImages)")
    
    Alamofire.request(.GET, request_url, parameters: ["method": request_method_id, "format": response_format, "nojsoncallback": 1, "api_key": api_key, "per_page": numberOfImages]).responseJSON { response in
        
        switch response.result {
            case .Success(let responseData):
                let jsonData = JSON(responseData)
            
                for index in 0...numberOfImages-1 {
                    
                    let flickrImage:FlickrObject = FlickrObject()
                    
                    flickrImage.id = jsonData["photos"]["photo"][index]["id"].intValue
                    flickrImage.owner = jsonData["photos"]["photo"][index]["owner"].stringValue
                    flickrImage.secret = jsonData["photos"]["photo"][index]["secret"].stringValue
                    flickrImage.server = jsonData["photos"]["photo"][index]["server"].intValue
                    flickrImage.farm = jsonData["photos"]["photo"][index]["farm"].intValue
                    
                    imageIDArray.append(flickrImage)
                }
            
            case .Failure(let error):
                print("Ruh roh! Error: \(error.description)")
        }
        
        // Once the GET is complete and the array is populated, we want to to create the image URL
        // This will be called 500+ times, so I'm putting it into a different queue
        dispatch_async(imageUtilsQueue) {
            for imageObj:FlickrObject in imageIDArray {
                imageURLToData(imageObj)
            }
        }
    }
}

// Create the url by hand, using the flickrobject from the previous method
func imageURLToData(imageObject: FlickrObject) {
    
    let lowResString = "https://farm\(imageObject.farm).staticflickr.com/\(imageObject.server)/\(imageObject.id)_\(imageObject.secret)_t.jpg"
    let highResString = "https://farm\(imageObject.farm).staticflickr.com/\(imageObject.server)/\(imageObject.id)_\(imageObject.secret)_b.jpg"
    
    let lowResURL = NSURL(string: lowResString)
    let highResURL = NSURL(string: highResString)
    
    let lowResData = NSData(contentsOfURL: lowResURL!)
    let highResData = NSData(contentsOfURL: highResURL!)
    
    debugPrint("Data created for \(imageObject.id)")
    
    saveImageToCoreData(imageObject.id, lowResData: lowResData!, highResData: highResData!)
}

//
//  FlickrImageObject.swift
//  PhotoFetcher
//
//  Created by Sumeet Gill on 2016-05-01.
//  Copyright © 2016 Sumeet Gill. All rights reserved.
//

import Foundation


class FlickrObject: NSObject {
    var id:Int = 0
    var owner:String = ""
    var secret:String = ""
    var server:Int = 0
    var farm:Int = 0
    
    var lowResURL:NSURL = NSURL()
    var highResURL:NSURL = NSURL()
}
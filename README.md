**This was for a coding assignment during the interview process of a company. I will not be doing further changes on this code.**

### How do I get set up? ###

### Summary of set up ###

* Pull and run it. The first run may not display any images because it is pulling and populating the database. Additional runs should load from CoreData just fine.

### Dependencies ###

* I have used 2 third party libraries so far. Alamofire for networking and SwiftyJSON for serialization

### Database configuration ###

* The application uses CoreData to store images. I have enabled "Allow External Storage" in case the image database requires it.

* The "LowResolution" entity has two attributes, *id* and *imageData*. LowResolution has an inverse relationship with HighResolution, which only has *imageData*

![Screen Shot 2016-05-04 at 00.49.02.png](https://bitbucket.org/repo/GB97xz/images/973330934-Screen%20Shot%202016-05-04%20at%2000.49.02.png)